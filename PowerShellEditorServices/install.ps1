$tempPath = [System.IO.Path]::GetTempPath()
$zipFile = "$(Join-Path $tempPath ([guid]::NewGuid())).zip"
$outPath = (Join-Path $PSScriptRoot "Bundle")
Invoke-Webrequest -Uri "https://github.com/PowerShell/PowerShellEditorServices/releases/latest/download/PowerShellEditorServices.zip" -OutFile $zipFile
Expand-Archive -Path $zipFile -DestinationPath $outPath
[Environment]::SetEnvironmentVariable("PS_EDITOR_SERVICE", $outPath, 'USER')
