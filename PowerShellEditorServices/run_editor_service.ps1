$PSES_BUNDLE_PATH = Join-Path $PSScriptRoot "Bundle"
$SESSION_TEMP_PATH = [System.IO.Path]::GetTempPath()
$SESSION_ID = [guid]::NewGuid()

$command = @(
    "$PSES_BUNDLE_PATH/PowerShellEditorServices/Start-EditorServices.ps1",
    "-BundledModulesPath $PSES_BUNDLE_PATH",
    "-LogPath $SESSION_TEMP_PATH/$($SESSION_ID)_logs.log",
    "-SessionDetailsPath $SESSION_TEMP_PATH/$($SESSION_ID)_session.json",
    "-FeatureFlags @()",
    "-AdditionalModules @()",
    "-HostName 'My Client'",
    "-HostProfileId 'myclient'",
    "-HostVersion 1.0.0",
    "-LogLevel Normal",
    "-Stdio"
)-join " "

pwsh -Command $command
